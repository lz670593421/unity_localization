using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;
using System;

[DefaultExecutionOrder(-10)]
public class LocaliztionManager : MonoBehaviour
{
    static public LocaliztionManager _instance;
    public enum languageSelect
    {
        en,
        ja
    }

    [SerializeField] TextAsset csvfile;
   const string fileName = "localizationTabel";
    [SerializeField] List<string[]> ui_textData = new List<string[]>();
    public List<string[]> UI_Data => ui_textData;
    [SerializeField] languageSelect language = languageSelect.en;
    public languageSelect Language => language;

    public List<TMP_FontAsset> fontAssets = new List<TMP_FontAsset>();
    public TMP_FontAsset CurrentFont { get; private set; }
    public int currentColumn { get; private set; }

    internal event Action onLangurageChanged;
    private void Awake()
    {
        if (_instance==null)
        {
            _instance = this;
        }
        else
        {
            Destroy(_instance);
        }
        string filePath = Application.dataPath + "/" + fileName + ".csv";
        //csvfile = Resources.Load("localizationTabel");
        StreamReader reader = new StreamReader(filePath);

        while (reader.Peek() != -1)
        {
            string line = reader.ReadLine();
            ui_textData.Add(line.Split(','));
        }
        reader.Close();
        ChangeLanguage(0);
    }

    /// <summary>
    /// Change Language by drop down select
    /// </summary>
    /// <param name="index">your language number</param>
    public void ChangeLanguage(int index)
    {
        //add you language and function here
        switch (index)
        {
            case 0:
                language = languageSelect.en;
                currentColumn = index + 1;
                ChangeFont(index);
                break;
            case 1:
                language = languageSelect.ja;
                currentColumn = index + 1;
                ChangeFont(index);
                break;
            case 2:
                break;
            default:
                language = languageSelect.en;
                currentColumn = index + 1;
                ChangeFont(index);
                break;
        }
        onLangurageChanged?.Invoke();
    }

    /// <summary>
    /// Debug function
    /// </summary>
    public void ShowText()
    {        
        switch (language) { 
            case languageSelect.en:
                Debug.Log(ui_textData[1][2]);
                break;
            case languageSelect.ja:
                Debug.Log(ui_textData[1][1]);
                break;
            default:
                break;
        }
    }

    void ChangeFont(int index)
    {
        if (CurrentFont == null || CurrentFont != fontAssets[index])
        {
            CurrentFont = fontAssets[index];
        }
    }
    public string GetTextByIndex(int RowIndex)
    {
        try
        {
            return ui_textData[RowIndex][currentColumn];
        }
        catch (Exception)
        {
            return "Can't find the text data!";
        }
    }
}
